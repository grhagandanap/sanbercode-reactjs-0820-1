// Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript"; 

console.log(kataPertama,kataKedua[0].toUpperCase()+kataKedua.slice(1),kataKetiga,kataKeempat.toUpperCase());

// Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "3";
var kataKeempat = "4"; 

console.log(Number(kataPertama)+Number(kataKedua)+Number(kataKetiga)+Number(kataKeempat));

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 10); 
var kataKetiga = kalimat.substr(15,3) ; 
var kataKeempat = kalimat.substr(19,5); 
var kataKelima = kalimat.substr(25,6); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4
var nilai = 75;

if (nilai >= 80) {
    console.log("A");
} else if (nilai >= 70 && nilai < 80) {
    console.log("B");
} else if (nilai >= 60 && nilai < 70) {
    console.log("C");
} else if (nilai >= 50 && nilai < 60) {
    console.log("D");
} else {
    console.log("E");
} 

// Soal 5
var tanggal = 1;
var bulan = 12;
var tahun = 1996;   

switch (bulan) {
    case 1: {console.log(String(tanggal),"Januari",String(tahun));}
    case 2: {console.log(String(tanggal),"Februari",String(tahun));}
    case 3: {console.log(String(tanggal),"Maret",String(tahun));}
    case 4: {console.log(String(tanggal),"April",String(tahun));}
    case 5: {console.log(String(tanggal),"Mei",String(tahun));}
    case 6: {console.log(String(tanggal),"Juni",String(tahun));}
    case 7: {console.log(String(tanggal),"Juli",String(tahun));}
    case 8: {console.log(String(tanggal),"Agustus",String(tahun));}
    case 9: {console.log(String(tanggal),"September",String(tahun));}
    case 10: {console.log(String(tanggal),"Oktober",String(tahun));}
    case 11: {console.log(String(tanggal),"November",String(tahun));}
    case 12: {console.log(String(tanggal),"Desember",String(tahun));}
}
