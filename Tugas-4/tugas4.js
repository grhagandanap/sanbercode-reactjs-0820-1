// Soal 1
var angka = 2;

console.log("// LOOPING PERTAMA");
while (angka < 21) {
    console.log(String(angka),"- I love coding");
    angka += 2;
    if (angka == 20) {
        console.log(String(angka),"- I love coding");
        console.log("// LOOPING KEDUA");
        while (angka > 1) {
            console.log(String(angka),"- I will become a frontend developer");
            angka -= 2;
        }
        break; 
    } 
}
console.log();

// Soal 2
for (var angka=1; angka <= 20; angka++) {
    if ((angka%2 != 0) && (angka%3 == 0)) {
        console.log(String(angka),"- I Love Coding");
    }  else if (angka%2 == 0) {
        console.log(String(angka),"- Berkualitas");
    } else {
        console.log(String(angka),"- Santai");
    }
}
console.log();

// Soal 3
var x = "";
for (let i=1; i<=7 ;i++) {
    x += "#";
    console.log(x);
}
console.log();

// Soal 4 
var kalimat = "saya sangat senang belajar javascript"
console.log(kalimat.split(" "));
console.log();

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
console.log(daftarBuah.sort().join("\n"));
