// Soal 1
const LuasLingkaran = (r) => {
    return 3.14*r**2;
}
let KelilingLingkaran = (r) => {
    return 3.14*r*2;
}
console.log(LuasLingkaran(4))
console.log(KelilingLingkaran(4))

// Soal 2
let kalimat = ""

const saya = "saya"
const adalah = "adalah"
const seorang = "seorang"
const frontend = "frontend"
const developer = "developer"

const tambahKata = (kata) => {
    return kalimat.concat(kata = `${saya} ${adalah} ${seorang} ${frontend} ${developer}`)
}
console.log(tambahKata(kalimat))

// Soal 3
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName() {
        console.log(firstName + " " + lastName)
      }
    }
}
newFunction("William", "Imoh").fullName() 

// Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined)
