var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Soal 2
readBooksPromise (7000, books[0])
    .then (x => readBooksPromise(x,books[1]))
    .then (x => readBooksPromise(x,books[2]))
    .catch (() => console.log())